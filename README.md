My simple stack in C, using an ADT Item
---------------------------------------

Only stack_pub.h is needed for the main to be compiled.

To compile the library:
<code>
gcc -c *c
ar -q libstack_typeYouWant *o
</code>

To use the library
<code>
gcc main.c -L. -lstack_typeYouWant -o main.out
</code>

For int: just use "int" folder ;)
For string: use "string" folder ;)
