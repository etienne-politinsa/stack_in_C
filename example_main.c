#include <stdio.h>
#include <stdlib.h>
#include "stack_pub.h"

int choice();

int main( void )
{
    Item pTop = initStack();
    int choix = -1;
    while (choix != 0)
    {
        choix = choice();
        switch (choix) {
            case 1:
                pTop = push(pTop);
                printStack(pTop);
                break;
            case 2:
                pTop = pop(pTop);
                printStack(pTop);
                break;
        }

    }

    destroyStack(pTop);
    printf("The stack has been emptied\n");
    return 0;
}

int choice()
{
    int choix;
    do{
        printf("1 - PUSH on the top of the stack\n");
        printf("2 - POP from the top of the stack\n");
        printf("0 - Destroy the stack\n");
        scanf("%d", &choix);
    } while( !(0 <= choix && 3 >= choix));
    
    return choix;
}
