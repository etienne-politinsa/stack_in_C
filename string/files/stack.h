#ifndef STACK_H
#define STACK_H
#include "stack_pub.h"

typedef struct item * Item;

Item newE();
Item initStack();
void destroyStack(Item pTop);
Item push(Item pOld);
void printStack(Item pTop);
Item pop(Item pTop);

/**********************************
 * To be define in a proper C file
 * Here it's stack_string.c
 *********************************/
void freeItem(Item pTop);
void printItem(Item pTop);
void setItem();

#endif
