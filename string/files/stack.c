#include <stdio.h>
#include <stdlib.h>
#include "stack.h"


Item newE()
{
    Item pTop = (struct item *)malloc(sizeof(*pTop));
    if (pTop == NULL){
        printf("Fail creating element");
        exit(-1);
    }
    setItem(pTop);
    pTop->next = NULL;
    return pTop;
}

Item initStack()
{
    Item pTop = NULL;
    return pTop;
}

void destroyStack(Item pTop)
{
    if (pTop == NULL)
    {
        return;
    }
    if (pTop->next == NULL){
        freeItem(pTop);
        return;
    }
    destroyStack(pTop->next);
    freeItem(pTop);
    return;
}

void printStack(Item pTop)
{
    printf("Stack :\t");
    if (pTop == NULL){
        printf("\nStack is empty\n\n");
        return;
    }

    while (pTop->next != NULL)
    {
        printItem(pTop);
        printf(" -> ");
        pTop = pTop->next;
    }
    printItem(pTop);
    printf("\n\n");
    fflush(stdout);
    return;
}

Item push(Item pOld)
{
    Item pTop = newE();
    pTop->next = pOld;
    return pTop;
}


Item pop(Item pTop)
{
    if (pTop == NULL){
        printf("Stack is empty\n");
        return NULL;
    }
    Item pOld;
    pOld = pTop;
    printf("\nOn pop ");
    printItem(pTop); printf("\n");   
    pTop = pTop->next;
    freeItem(pOld);
    return pTop;
}
