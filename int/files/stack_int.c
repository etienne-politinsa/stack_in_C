#include <stdio.h>
#include <stdlib.h>
#include "stack.h"


void freeItem(Item pTop)
{
    free(pTop);
    return;
}

void printItem(Item pTop)
{
    printf("%d", pTop->val);
    return;
}

void setItem(Item pTop)
{
    int key;
    printf("Valeur de la key : ");
    scanf("%d", &key);
    pTop->val = key;
    printf("\n");
    return;
}

