#ifndef STACK_PUB_H
#define STACK_PUB_H
#include <stdlib.h>
#include <stdio.h>

typedef struct item * Item;
struct item{
    int val;
    struct item *next;
};

Item initStack();
void destroyStack(Item pTop);
void printStack(Item pTop);
Item push(Item pOld);
Item pop(Item pTop);

#endif
